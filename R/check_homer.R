#' Check for working HOMER installation
#'
#' Used to check that HOMER is installed and working on package attach,
#' and reads/sets the \code{options('homer_path')} if user has not.
#'
#' @return logical; \code{TRUE} for success, \code{FALSE} otherwise
#' @export
check_homer <- function() {
    # get_homer_bin now almost always finds homer via the PATH variable on linux
    cmd <- paste0(get_homer_bin(), "/findMotifsGenome.pl")
    output <- suppressWarnings(system(cmd, ignore.stdout = TRUE, ignore.stderr = TRUE))
    ## Success - found in PATH
    if (output == 0) {
        message("HOMER installation found.")
        # this is an complementary approach to get_homer_bin which should produce identical results
        # remove -a parameter from type command as it breaks on many systems
        loc <- system(paste('type', cmd), intern = TRUE) 
        tmp <- stringr::str_split(loc, ' ', simplify = T)[3]
        hb <- dirname(tmp) # the .../homer/bin directory
        # gsub here removes trailing slash like 'dirname'
        if(!identical(hb, gsub("/$", "", get_homer_bin()))){
            message(paste0("Conflicting homer paths found. ",
                           "get_homer_bin() returned ", 
                           get_homer_bin(),
                           " while system 'type' command found ",
                           hb))
        }
        hp <- dirname(hb) # the path to homer (parent of the homer/bin)
        options('homer_path' = hp)
        # add homer/bin to the path...
        Sys.setenv(PATH = paste0(get_homer_bin(), ":", Sys.getenv("PATH")))
        return(invisible(0))
    }

    ## Not a default success, but user has set `options('homer_path')` (success or fail)
    if (output != 0 && !is.null(options('homer_path')$homer_path)) {
        homer_base <- options('homer_path')
        cmd2 <- paste0(homer_base, '/bin/findMotifsGenome.pl')
        output2 <- suppressWarnings(system(cmd2, ignore.stdout = TRUE, ignore.stderr = TRUE))
        
        if (output2 == 0) {
            message("HOMER installation found via options('homer_path')")
            message("Amending R's $PATH via `Sys.setenv` to add all HOMER utils ..")

            homer_bin <- paste0(homer_base$homer_path, '/bin')
            path.env <- Sys.getenv('PATH')
            Sys.setenv('PATH' = paste0(homer_bin, ':', path.env))

            ## Recheck if amending worked
            output2 <- suppressWarnings(system(cmd2, 
                                               ignore.stdout = TRUE, 
                                               ignore.stderr = TRUE))
            output3 <- suppressWarnings(system('findKnownMotifs.pl', 
                                               ignore.stdout = TRUE, 
                                               ignore.stderr = TRUE))

            if (output2 == 0 & output3 == 0) {
                message("Successfully added HOMER utils.")
                return(invisible(0))
            } else {
                message(paste0("Failed to load HOMER utils. Check your HOMER install, ", 
                               "and failing that, file a Github issue."))
                return(invisible(127))
            }
        } else {
            message("HOMER installation not found.\n")
            message(paste0("Check `options('homer_path')` and that HOMER is properly installed.",
                    " Be sure to set the path to point to the base directory of",
                    " your HOMER install.\n"))
            return(invisible(127))
        }
    }

    ## Failure - not findable by default and not set `options('homer_path)`
    if (output != 0 & is.null(options('homer_path')$homer_path)) {
        message("HOMER installation not found.\n")
        message(paste("Make sure HOMER is properly installed on your system.",
                      "See http://homer.ucsd.edu/homer/index.html",
                      "",
                      "Following that, the first and safest fix is to add the following",
                      "line in your ~/.Rprofile (or run it interactively upon starting R):",
                      "",
                      "options('homer_path' = '/path/to/homer')",
                      "",
                      "If `check_homer()` still doesn't work, then HOMER may not be in your $PATH.",
                      "(e.g. your system cannot find the HOMER utils)",
                      "Add HOMER to your PATH by adding the following line to your ~/.bashrc:",
                      "",
                      "PATH=$PATH:/your/path/to/homer/bin/",
                      "",
                      "Then `source ~/.bashrc` to add HOMER to your path.",
                      "",
                      "If still no good, file an issue on Github:",
                      "https://github.com/robertamezquita/marge",
                      sep = '\n'))
        return(invisible(127))
    }
}

#' Get the base installation directory of HOMER
#'
#' Grabs the base directory of HOMER installation from \code{options('homer_path')};
#' the correct base path should be set with package attachment, or alert user
#' to check/set the path manually. This function is kept to keep code from breaking.
#' 
#' @param path character string indicating the location of the HOMER base directory.
#' If `path = NULL` then the function checks if `options('homer_path')` is
#' set, if this also returns null then the function checks both `.bashrc` and 
#' `.bash_profile` for the `PATH` variable. If all options fail then the function
#' falls back on the best guess for the user's local installation bin (on linux) 
#' which is `'/usr/local/bin/'`.
#' 
#' @return character vector with base directory of HOMER installation
#' @export
get_homer_bin <- function(path = NULL) {
    if(is.null(path)){
        if(is.null(options('homer_path')$homer_path)){
            # check if we can find it in the PATH
            homer_path <- NULL
            if(file.exists("~/.bashrc")){
                if(length(suppressWarnings(system("grep '^PATH' ~/.bashrc", intern = T))) != 0){
                    homer_path <- system("grep '^PATH' ~/.bashrc", intern = T) %>% 
                        stringr::str_split(., ":", simplify = T) %>% 
                        grep("homer", ., value = T, ignore.case = T)
                }
            } 
            if(file.exists("~/.bash_profile") && is.null(homer_path)){
                if(length(suppressWarnings(system("grep '^PATH' ~/.bash_profile", intern = T))) != 0){
                    homer_path <- system("grep '^PATH' ~/.bash_profile", intern = T) %>% 
                        stringr::str_split(., ":", simplify = T) %>% 
                        grep("homer", ., value = T, ignore.case = T)
                }
            }
            if(is.null(homer_path)){
                # choose a rational location for homer links?
                homer_bin <- '/usr/local/bin/'
            } else {
                homer_bin <- homer_path
            }
        } else {
            homer_base <- options('homer_path')$homer_path
            homer_bin <- paste0(homer_base, '/bin/')
        }
    } else {
        homer_bin <- paste0(path, '/bin/')
    }
    return(homer_bin)
}


#' List installed/available HOMER packages
#'
#' Runs the configureHomer.pl script to get list of installed and
#' available packages, parsing the output into a tidy tibble.
#'
#' @param installed_only only show installed packages (default: TRUE)
#' 
#' @return \code{tibble} with status of installed (+) or not (-),
#'   the package, version, and description.
#' @export
list_homer_packages <- function(installed_only = TRUE) {
    homer_base <- dirname(get_homer_bin())

    homer_conf <- paste0(homer_base, '/configureHomer.pl')

    if (!file.exists(homer_conf)) {
        stop(paste("The configureHomer.pl script was not found at the",
                   "base of the HOMER directory:",
                   "",
                   homer_base,
                   "",
                   "Place the configureHomer.pl script in the above directory.",
                   sep = '\n'))
    }

    cmd <- paste('perl', homer_conf, '-list')
    output <- system(cmd, intern = TRUE, ignore.stderr = TRUE) %>%
        .[stringr::str_detect(., "v[0-9]")] %>% 
        stringr::str_split(., "\t", simplify = T) %>% 
        data.frame()
    colnames(output) <- c("status", "package", "version", "description")
    
    ## Return either only installed or all packages
    if (installed_only) {
        return(dplyr::filter(output, status == '+'))
    } else {
        return(output)
    }
}

#' Install a HOMER package
#' 
#' @description Install a homer package from the available options
#' @param package character string specifying a valid homer package to install
#' @return Nothing, the side effect is to install the specified homer package
#' @export
install_homer_package <- function(package){
    if(missing(package)){
        stop("package is missing with no default.")
    }
    homer_base <- dirname(get_homer_bin())
    homer_conf <- paste0(homer_base, '/configureHomer.pl')
    if (!file.exists(homer_conf)) {
        stop(paste("The configureHomer.pl script was not found at the",
                   "base of the HOMER directory:",
                   "",
                   homer_base,
                   "",
                   "Place the configureHomer.pl script in the above directory.",
                   sep = '\n'))
    }
    cmd <- paste('perl', homer_conf, '-list')
    avail <- system(cmd, intern = TRUE, ignore.stderr = TRUE) %>% 
        .[stringr::str_detect(., "v[0-9]")] %>% 
        stringr::str_split(., "\t", simplify = T) %>% 
        .[-1,2]
    # check that package is available
    package <- match.arg(package, avail)
    print(paste0("Installing requested package: ", package, ". This can take several minutes."))
    # do not allow std.err to print or it will crash rstudio
    system(paste('perl', homer_conf, '-install', package), ignore.stderr = TRUE)
}


